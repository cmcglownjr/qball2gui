# QBall2 GUI

## Introduction
This is a Graphical User Interface(GUI) wrapper for a Fortran code called QBALL2. The original Fortran code was written 
by Vijay N. Madi of Armco Research & Technology in 1994. This project was initiated by a client who needs to run the 
Fortran code on newer machines. The original code was compiled to run on both Windows and Linux. That executable is 
called from the GUI, given the necessary inputs, and it's output is processed by the GUI program. The data is then 
organized into a csv and Excel format.

## Getting Started

## Usage

## Screenshots

## License
Armco Research & Technology holds all rights to the original Fortran source code.
## Acknowledgements
I would like to acknowledge Vijay N. Madi of Armco Research & Technology for the original work.