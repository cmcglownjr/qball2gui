import os
import sys
import logging
from abc import ABC, abstractmethod
from pathlib import Path

import lib.utilities

logger = lib.utilities.create_logger(__name__)


def check_if_directory(dir_str: str) -> str:
    if not os.path.isdir(dir_str):
        raise NotADirectoryError(f'The path "{dir_str}" is not a valid directory.')
    return dir_str


class DatFiles(ABC):
    @abstractmethod
    def __init__(self, workdir: Path, *args, **kwargs):
        self._workdir = None
        self._filename = None
        self.workdir = workdir

    @property
    def workdir(self) -> str:
        return self._workdir

    @workdir.setter
    def workdir(self, workdir_value: str) -> None:
        try:
            self._workdir = check_if_directory(workdir_value)
        except NotADirectoryError as err:
            logger.exception(err)
            sys.exit(1)

    @classmethod
    def create_file(cls, _workdir: Path, _filename: str) -> None:
        filepath = os.path.join(os.path.join(_workdir, 'DATFiles'), _filename)
        if os.path.exists(filepath):
            logger.info(f'The file "{_filename}" already exists. Overwriting...')
            os.remove(filepath)
        f = open(filepath, 'x')
        f.close()


class InputDATFile(DatFiles):
    def __init__(self, workdir: Path, input_data: dict) -> None:
        super().__init__(workdir, input_data)
        self. _workdir = self._input_data = None
        self.workdir = workdir
        self.input_data = input_data
        self._filename = 'INPUT.DAT'

    @property
    def input_data(self) -> dict:
        return self._input_data

    @input_data.setter
    def input_data(self, input_data_value: dict) -> None:
        dict_keys = ['uom', 'uot', 'bdia', 'taust', 'pqctype', 'act', 'stemp', 'qtemp', 'placeholder_1', 'qtime', 'eqt',
                     'placeholder_2', 'placeholder_3', 'pqtime', 'phtemp', 'phtime', 'pttime', 'placeholder_4', 'dist1',
                     'dist2', 'dist3', 'mstemp', 'iter', 'incr']
        for i in input_data_value:
            if i not in dict_keys:
                raise KeyError(f'You are missing a key, "{i}" is not a valid key.')
        self._input_data = input_data_value

    def create_file(self, **kwargs):
        super().create_file(self._workdir, self._filename)
        with open(os.path.join(os.path.join(self._workdir, 'DATFiles'), self._filename), 'w+') as dat_file:
            for i in self._input_data:
                dat_file.write(str(self._input_data[i]))
                dat_file.write('\r\n')


class ITTFile(DatFiles):
    def __init__(self, workdir: Path) -> None:
        super().__init__(workdir)
        self._workdir = None
        self.workdir = workdir
        self._filename = 'ITT.DAT'

    def __list_to_string(self, data: list) -> str:
        output = ''
        for i in range(0, len(data)):
            output += str(data[i]) + ' '
        return output

    def create_file(self, steel_chem: list[float] = None, iso_trans_tem: list[list[float]] = None) -> None:
        super().create_file(self._workdir, self._filename)
        if steel_chem is None:
            steel_chem = [0.8, 0.9, 0.2, 0.6, 0.0, 0.07]
        if iso_trans_tem is None:
            iso_trans_tem = [
                [1300, 115, 300, 1230],
                [1250, 50, 100, 200],
                [1200, 22, 50, 90],
                [1150, 14.6, 30.3, 60],
                [1100, 10.7, 24.6, 50],
                [1050, 9, 23, 46],
                [1000, 8, 23, 46]
            ]
        with open(os.path.join(os.path.join(self._workdir, 'DATFiles'), self._filename), 'w+') as dat_file:
            dat_file.write(self.__list_to_string(steel_chem))
            dat_file.write('\r\n')
            dat_file.write(str(len(iso_trans_tem)))
            dat_file.write('\r\n')
            for i in range(0, len(iso_trans_tem)):
                dat_file.write(self.__list_to_string(iso_trans_tem[i]))
                dat_file.write('\r\n')


class LTemFile(DatFiles):
    def __init__(self, workdir: Path) -> None:
        self._workdir = None
        self.workdir = workdir
        self._filename = 'LTEMP.DAT'

    def create_file(self, **kwargs) -> None:
        super().create_file(self._workdir, self._filename)


class PhaseFile(DatFiles):
    def __init__(self, workdir: Path) -> None:
        self._workdir = None
        self.workdir = workdir
        self._filename = 'PHASE.DAT'

    def create_file(self, **kwargs) -> None:
        super().create_file(self._workdir, self._filename)


class StressFile(DatFiles):
    def __init__(self, workdir: Path) -> None:
        self._workdir = None
        self.workdir = workdir
        self._filename = 'STRESS.DAT'

    def create_file(self, **kwargs) -> None:
        super().create_file(self._workdir, self._filename)


class TempFile(DatFiles):
    def __init__(self, workdir: Path) -> None:
        self._workdir = None
        self.workdir = workdir
        self._filename = 'TEMP.DAT'

    def create_file(self, **kwargs) -> None:
        super().create_file(self._workdir, self._filename)
