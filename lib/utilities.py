import os
import logging
from pathlib import Path


def create_logger(logger_name: logging.getLogger()) -> logging.Logger:
    base_dir = Path(__file__).resolve().parent.parent
    if not os.path.exists(os.path.join(base_dir, 'log')):
        os.mkdir(os.path.join(base_dir, 'log'))
    # Set up logger
    logger = logging.getLogger(logger_name)
    logger.setLevel(logging.DEBUG)
    log_format = logging.Formatter('%(asctime)s:%(name)s:%(levelname)s:%(message)s', datefmt='%m/%d/%Y %I:%M:%S %p')
    # Set handlers
    # Stream handler streams to terminal
    stream_handler = logging.StreamHandler()
    stream_handler.setFormatter(log_format)
    logger.addHandler(stream_handler)
    # File handler saves to file
    log_dir = os.path.join(base_dir, 'log')
    file_handler = logging.FileHandler(os.path.join(log_dir, 'QBall2.log'), mode='a')
    file_handler.setFormatter(log_format)
    logger.addHandler(file_handler)
    return logger
