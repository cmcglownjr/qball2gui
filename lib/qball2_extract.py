import os
import sys
import re
import logging
from abc import ABC, abstractmethod
from pathlib import Path
from openpyxl import Workbook
from openpyxl.worksheet import worksheet

import lib.utilities

logger = lib.utilities.create_logger(__name__)
digits = re.compile(r'(\d*\.\d*)')
words = re.compile(r'[A-Z]+')


def fill_sheet_row(sheet: worksheet, row: int, data: list) -> None:
    for i in range(0, len(data)):
        sheet.cell(row=row, column=i+1).value = data[i]


class TempProcess:
    def __init__(self, workbook: Workbook = None, datfile: str = None) -> None:
        self.workbook = workbook
        self.datfile = datfile
        self.sheet_name = 'Temperature'

    def read_datfile(self) -> Workbook:
        row_number = 2
        self.workbook.create_sheet(self.sheet_name)
        logger.info(f'Creating worksheet {self.sheet_name}.')
        sheet = self.workbook[self.sheet_name]
        with open(self.datfile, 'r+') as datfile:
            first = datfile.readline()
            loc1, loc2, loc3 = digits.findall(first)
            sheet_data = [
                'Time (sec)',
                f'{loc1} in. from center',
                f'{loc2} in. from center',
                f'{loc3} in. from center'
            ]
            fill_sheet_row(sheet, 1, sheet_data)
            for i, line in enumerate(datfile):
                if i > 19:
                    if '-' in line:
                        break
                    t, l1, l2, l3 = digits.findall(line)
                    fill_sheet_row(sheet, row_number, [float(t), float(l1), float(l2), float(l3)])
                    row_number += 1
        return self.workbook


class PhaseProcess:
    def __init__(self, workbook: Workbook = None, datfile: str = None) -> None:
        self.workbook = workbook
        self.datfile = datfile
        self.sheet_name = 'Phase'

    def read_datfile(self) -> Workbook:
        row_number = 2
        self.workbook.create_sheet(self.sheet_name)
        logger.info(f'Creating worksheet {self.sheet_name}.')
        sheet = self.workbook[self.sheet_name]
        sheet_data = ['Depth', 'Temp', 'Aust', 'Mart', 'Perl', 'Bain']
        fill_sheet_row(sheet, 1, sheet_data)
        with open(self.datfile, 'r+') as datfile:
            for i, line in enumerate(datfile):
                if i > 3:
                    if 'SURFACE' in line or 'CENTER' in line:
                        depth = words.findall(line)[0]
                        temp, aust, mart, perl, bain = digits.findall(line)
                        fill_sheet_row(sheet, row_number,
                                       [str(depth), float(temp), float(aust), float(mart), float(perl), float(bain)])
                    else:
                        depth, temp, aust, mart, perl, bain = digits.findall(line)
                        fill_sheet_row(sheet, row_number,
                                       [float(depth), float(temp), float(aust), float(mart), float(perl), float(bain)])
                    row_number += 1
        return self.workbook


class StressProcess:
    def __init__(self, workbook: Workbook = None, datfile: str = None) -> None:
        self.workbook = workbook
        self.datfile = datfile
        self.sheet_name = 'Stress'

    def read_datfile(self) -> Workbook:
        row_number = 3
        self.workbook.create_sheet(self.sheet_name)
        logger.info(f'Creating worksheet {self.sheet_name}.')
        sheet = self.workbook[self.sheet_name]
        sheet_data = ['Radius', 'Area', 'Circumference', 'Radial', 'ABS', 'Yield']
        fill_sheet_row(sheet, 2, sheet_data)
        with open(self.datfile, 'r+') as datfile:
            line = datfile.readline()
            fill_sheet_row(sheet, 1, [line.strip()])
            for i, line in enumerate(datfile):
                if i > 0:
                    radius, area, circum, radial, s_abs, s_yield = digits.findall(line)
                    fill_sheet_row(sheet, row_number, [float(radius), float(area), float(circum), float(radial),
                                                       float(s_abs), float(s_yield)])
                    row_number += 1
        return self.workbook


class LTempProcess:
    def __init__(self, workbook: Workbook = None, datfile: str = None) -> None:
        self.workbook = workbook
        self.datfile = datfile
        self.sheet_name = 'LTemp'

    def read_datfile(self) -> Workbook:
        row_number = 1
        self.workbook.create_sheet(self.sheet_name)
        logger.info(f'Creating worksheet {self.sheet_name}.')
        sheet = self.workbook[self.sheet_name]
        with open(self.datfile, 'r+') as datfile:
            line = datfile.readline()
            fill_sheet_row(sheet, row_number, [line.strip()])
            row_number += 1
        return self.workbook
