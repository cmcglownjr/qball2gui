import os
import sys
import subprocess
import logging
from pathlib import Path
from sys import platform
from PySide2 import QtCore, QtWidgets
from PySide2.QtWidgets import QApplication, QMainWindow, QVBoxLayout, QFormLayout, QHBoxLayout, QSpacerItem, QWidget
from PySide2.QtWidgets import QTextBrowser, QPushButton, QComboBox, QLineEdit, QStatusBar, QMenuBar, QLabel, QAction
from PySide2.QtWidgets import QFileDialog
from PySide2.QtCore import QFile, Qt, QSize
from PySide2.QtGui import QIcon, QFont, QPixmap
from openpyxl import Workbook

from lib.utilities import create_logger
from lib.qball2_extract import TempProcess, PhaseProcess, StressProcess, LTempProcess
from lib.qball2_files import InputDATFile, ITTFile, LTemFile, PhaseFile, StressFile, TempFile


logger = create_logger(__name__)
degree_sign = u'\N{DEGREE SIGN}'


class GuiLogger(logging.Handler):
    def emit(self, record) -> None:
        self.edit.append(self.format(record))


class MainWindow(QMainWindow):
    def __init__(self):
        super().__init__()
        self.input_dict: dict = {
            'uom': 'in',
            'uot': 'F',
            'bdia': 6,
            'taust': 1550,
            'pqctype': 0,
            'act': 10,
            'stemp': 1300,
            'qtemp': 120,
            'placeholder_1': 0,
            'qtime': 300,
            'eqt': 0,
            'placeholder_2': 0,
            'placeholder_3': 0,
            'pqtime': 400,
            'phtemp': 0,
            'phtime': 0,
            'pttime': 0,
            'placeholder_4': 0,
            'dist1': 3.0,
            'dist2': 1.5,
            'dist3': 0.0,
            'mstemp': 410,
            'iter': 0,
            'incr': 1
        }
        self.base_dir = os.getcwd()
        self.setWindowTitle('QBall Quench Analysis')
        self.setFixedSize(QSize(800, 550))
        self.create_menu()
        # Widgets
        self.menu_bar: QMenuBar = QMenuBar()
        self.btn_run: QPushButton = QPushButton('Run')
        self.cbo_uom: QComboBox = QComboBox()
        self.cbo_uom.addItem('in', userData='in')
        self.cbo_uom.addItem('mm', userData='mm')
        self.cbo_uot: QComboBox = QComboBox()
        self.cbo_uot.addItem('F', userData='F')
        self.cbo_uot.addItem('C', userData='C')
        self.cbo_iter: QComboBox = QComboBox()
        self.cbo_iter.addItem('No', userData='No')
        self.cbo_iter.addItem('Yes', userData='Yes')
        self.cbo_incr: QComboBox = QComboBox()
        self.cbo_incr.addItem('1', userData='1')
        self.cbo_incr.addItem('10', userData='10')
        self.cbo_incr.addItem('100', userData='100')
        self.ln_bdia: QLineEdit = QLineEdit()
        self.ln_taust: QLineEdit = QLineEdit()
        self.ln_act: QLineEdit = QLineEdit()
        self.ln_stemp: QLineEdit = QLineEdit()
        self.ln_qtemp: QLineEdit = QLineEdit()
        self.ln_qtime: QLineEdit = QLineEdit()
        self.ln_pqtime: QLineEdit = QLineEdit()
        self.ln_phtemp: QLineEdit = QLineEdit()
        self.ln_phtime: QLineEdit = QLineEdit()
        self.ln_pttime: QLineEdit = QLineEdit()
        self.ln_dist1: QLineEdit = QLineEdit()
        self.ln_dist2: QLineEdit = QLineEdit()
        self.ln_dist3: QLineEdit = QLineEdit()
        self.ln_mstemp: QLineEdit = QLineEdit()
        self.tbrowser_log: QTextBrowser = QTextBrowser()
        self.status: QStatusBar = QStatusBar()
        self.menu: QMenuBar = QMenuBar()

        self.set_placeholder_text()
        # Layout
        widget = QWidget()
        widget.setLayout(self.create_layout())
        self.setCentralWidget(widget)
        # Log to text browser
        h = GuiLogger()
        h.edit = self.tbrowser_log
        logging.getLogger().addHandler(h)
        # Signals
        self.btn_run.clicked.connect(self.run_clicked)
        self.cbo_uot.currentIndexChanged.connect(self.units_of_measurement)
        self.cbo_uot.currentIndexChanged.connect(self.units_of_temperature)
        self.cbo_incr.currentIndexChanged.connect(self.incr_changed)
        self.cbo_iter.currentIndexChanged.connect(self.iter_changed)

    def create_menu(self):
        main_menu: QMenuBar = self.menuBar()
        file_menu = main_menu.addMenu('File')
        # options_menu = main_menu.addMenu('Options')
        # help_menu = main_menu.addMenu('Help')

        new_action: QAction = QAction(QIcon("Resource/Qball-New-256.png"), 'New', self)
        new_action.setShortcut('Ctrl+N')
        open_action: QAction = QAction(QIcon("Resource/Qball-Open-256.png"), 'Open', self)
        open_action.setShortcut('Ctrl+O')
        save_action: QAction = QAction(QIcon("Resource/Qball-Save-256.png"), 'Save', self)
        save_action.setShortcut('Ctrl+S')
        save_as_action: QAction = QAction(QIcon("Resource/Qball-Save-as-256.png"), 'Save As', self)
        save_as_action.setShortcut('Shift+Ctrl+S')
        quit_action: QAction = QAction(QIcon("Resource/Qball-Quit-256.png"), 'Quit', self)
        quit_action.setShortcut('Ctrl+Q')
        quit_action.triggered.connect(self.exit_app)
        # create_itt_action: QAction = QAction(QIcon("Resource/Qball-Create-256.png"), 'ITT File', self)
        # create_itt_action.triggered.connect(self.itt_clicked)
        about_action: QAction = QAction(QIcon("Resource/Qball-Question-256.png"), 'About', self)
        # about_action.triggered.connect()

        # file_menu.addAction(new_action)
        # file_menu.addAction(open_action)
        # file_menu.addAction(save_action)
        # file_menu.addAction(save_as_action)
        # file_menu.addSeparator()
        file_menu.addAction(quit_action)

        # options_menu.addAction(create_itt_action)

        # help_menu.addAction(about_action)

    def create_layout(self) -> QVBoxLayout:
        main_layout: QVBoxLayout = QVBoxLayout()
        h_layout_version: QHBoxLayout = QHBoxLayout()
        lbl_version: QLabel = QLabel()
        lbl_version.setText('Version 1.0.0')
        version_spacer: QSpacerItem = QSpacerItem(40, 20, QtWidgets.QSizePolicy.Policy.Expanding)
        h_layout_version.addSpacerItem(version_spacer)
        h_layout_version.addWidget(lbl_version)
        main_layout.addLayout(h_layout_version)

        lbl_title: QLabel = QLabel()
        lbl_title.setText('QBall Quench Analysis')
        lbl_title.setFont(QFont('Monospace', 24))
        lbl_title.setAlignment(Qt.AlignCenter)
        main_layout.addWidget(lbl_title)

        input_layout: QHBoxLayout = QHBoxLayout()
        form_left: QFormLayout = QFormLayout()
        form_right: QFormLayout = QFormLayout()
        form_left.addRow(QLabel('Measurment Units'), self.cbo_uom)
        form_left.addRow(QLabel('Temperature Units'), self.cbo_uot)
        form_left.addRow(QLabel('Ball Diameter'), self.ln_bdia)
        form_left.addRow(QLabel('Austenitizing Temperature'), self.ln_taust)
        form_left.addRow(QLabel('Air Cool Time (ACT)'), self.ln_act)
        form_left.addRow(QLabel('Surface Temperature'), self.ln_stemp)
        form_left.addRow(QLabel('Quench Water Temperature'), self.ln_qtemp)
        form_left.addRow(QLabel('In Quench Time'), self.ln_qtime)
        form_left.addRow(QLabel('Post Quench ACT'), self.ln_pqtime)

        form_right.addRow(QLabel('Tempering Temperature'), self.ln_phtemp)
        form_right.addRow(QLabel('Tempering Time'), self.ln_phtime)
        form_right.addRow(QLabel('Post-Temper ACT'), self.ln_pttime)
        form_right.addRow(QLabel('Distance 1'), self.ln_dist1)
        form_right.addRow(QLabel('Distance 2'), self.ln_dist2)
        form_right.addRow(QLabel('Distance 3'), self.ln_dist3)
        form_right.addRow(QLabel('MS Temperature'), self.ln_mstemp)
        form_right.addRow(QLabel('ITER'), self.cbo_iter)
        form_right.addRow(QLabel('Print Increment'), self.cbo_incr)
        input_layout.addLayout(form_left)
        input_layout.addLayout(form_right)
        input_layout.addWidget(self.btn_run)
        main_layout.addLayout(input_layout)

        main_layout.addWidget(self.tbrowser_log)
        main_layout.addWidget(self.status)

        return main_layout

    def exit_app(self):
        self.close()

    def run_clicked(self):
        try:
            self._fill_input_dict()
            self.create_dat_files()
            wb = self.create_excel()
            documents = os.path.expanduser('~/Documents')
            save_file, _filter = QFileDialog.getSaveFileName(self, "Save as Excel File", documents,
                                                             "Excel Files (*.xlsx)",
                                                             selectedFilter='*.xlsx')
            if '.xls' not in save_file or '.xlsx' not in save_file:
                save_file += '.xlsx'
            if save_file is not None:
                wb.save(save_file)
                logger.info(f'Workbook saved to {save_file}')
        except ValueError:
            logger.warning('You must fill in the correct values.')

    def _fill_input_dict(self):
        sys.tracebacklimit = 0
        try:
            if self.ln_bdia.text() != '':
                self.input_dict.update({'bdia': float(self.ln_bdia.text())})
        except ValueError as err:
            logger.exception('The value you put in for the diameter has to be a number!')
            raise ValueError(err)
        try:
            if self.ln_taust.text() != '':
                self.input_dict.update({'taust': int(self.ln_taust.text())})
        except ValueError as err:
            logger.warning('The value you put in for the Austenitizing Temperature has to be a number with no '
                           'decimals!')
            raise ValueError(err)
        try:
            if self.ln_act.text() != '':
                self.input_dict.update({'act': int(self.ln_act.text())})
        except ValueError as err:
            logger.warning('The value you put in for the Air Cool Time has to be a number with no decimals!')
            raise ValueError(err)
        try:
            if self.ln_stemp.text() != '':
                self.input_dict.update({'stemp': int(self.ln_stemp.text())})
        except ValueError as err:
            logger.warning('The value you put in for the Surface Temperature has to be a number with no decimals!')
            raise ValueError(err)
        try:
            if self.ln_qtemp.text() != '':
                self.input_dict.update({'qtemp': int(self.ln_qtemp.text())})
        except ValueError as err:
            logger.warning('The value you put in for the Quench Water Temperature has to be a number with no decimals!')
            raise ValueError(err)
        try:
            if self.ln_qtime.text() != '':
                self.input_dict.update({'qtime': int(self.ln_qtime.text())})
        except ValueError as err:
            logger.warning('The value you put in for the In Quench Time has to be a number with no decimals!')
            raise ValueError(err)
        try:
            if self.ln_pqtime.text() != '':
                self.input_dict.update({'pqtime': int(self.ln_pqtime.text())})
        except ValueError as err:
            logger.warning('The value you put in for the Post Quench ACT has to be a number with no decimals!')
            raise ValueError(err)
        try:
            if self.ln_phtemp.text() != '':
                self.input_dict.update({'phtemp': int(self.ln_phtemp.text())})
        except ValueError as err:
            logger.warning('The value you put in for the Tempering Temperature has to be a number with no decimals!')
            raise ValueError(err)
        try:
            if self.ln_phtime.text() != '':
                self.input_dict.update({'phtime': int(self.ln_phtime.text())})
        except ValueError as err:
            logger.warning('The value you put in for the Tempering Time has to be a number with no decimals!')
            raise ValueError(err)
        try:
            if self.ln_pttime.text() != '':
                self.input_dict.update({'pttime': int(self.ln_pttime.text())})
        except ValueError as err:
            logger.warning('The value you put in for the Post-Temper ACT has to be a number with no decimals!')
            raise ValueError(err)
        try:
            if self.ln_dist1.text() != '':
                self.input_dict.update({'dist1': float(self.ln_dist1.text())})
        except ValueError as err:
            logger.warning('The value you put in for the Distance 1 has to be a number!')
            raise ValueError(err)
        try:
            if self.ln_dist2.text() != '':
                self.input_dict.update({'dist2': float(self.ln_dist2.text())})
        except ValueError as err:
            logger.warning('The value you put in for the Distance 2 has to be a number!')
            raise ValueError(err)
        try:
            if self.ln_dist3.text() != '':
                self.input_dict.update({'dist3': float(self.ln_dist3.text())})
        except ValueError as err:
            logger.warning('The value you put in for the Distance 3 has to be a number!')
            raise ValueError(err)
        try:
            if self.ln_mstemp.text() != '':
                self.input_dict.update({'mstemp': int(self.ln_mstemp.text())})
        except ValueError as err:
            logger.warning('The value you put in for the MS Temperature has to be a number with no decimals!')
            raise ValueError(err)

    def set_placeholder_text(self, imperial: bool = True):
        if imperial:
            temperature = 'F'
        else:
            temperature = 'C'
        self.ln_bdia.setPlaceholderText('6" or 152mm Max')
        self.ln_taust.setPlaceholderText(f'>1475 {degree_sign}F or >802 {degree_sign}C')
        self.ln_act.setPlaceholderText('sec.')
        self.ln_stemp.setPlaceholderText('Pre-quench surface temp (1300)')
        self.ln_qtemp.setPlaceholderText(f'{degree_sign}{temperature}')
        self.ln_qtime.setPlaceholderText('sec')
        self.ln_pqtime.setPlaceholderText('sec')
        self.ln_phtemp.setPlaceholderText(f'{degree_sign}{temperature}')
        self.ln_phtime.setPlaceholderText('sec')
        self.ln_pttime.setPlaceholderText('sec')
        self.ln_mstemp.setPlaceholderText(f'{degree_sign}{temperature}')
        self.ln_dist1.setPlaceholderText('Distance from center: 3in')
        self.ln_dist2.setPlaceholderText('Distance from center: 1.5in')
        self.ln_dist3.setPlaceholderText('Distance from center: 0in')

    def units_of_measurement(self):
        uom: str = self.cbo_uom.itemData(self.cbo_uom.currentIndex())
        self.input_dict.update({'uom': uom})
        logger.info(f'Units of measure set to {uom}.')

    def units_of_temperature(self):
        uot: str = self.cbo_uot.itemData(self.cbo_uot.currentIndex())
        self.input_dict.update({'uot': uot})
        logger.info(f'Units of temperature set to {degree_sign}{uot}.')
        if uot == 'C':
            self.set_placeholder_text(imperial=False)
        else:
            self.set_placeholder_text(imperial=True)

    def iter_changed(self):
        iter_value: str = self.cbo_iter.itemData(self.cbo_iter.currentIndex())
        if iter_value == 'Yes':
            self.input_dict.update({'iter': 1})
        else:
            self.input_dict.update({'iter': 0})
        logger.info(f'Final ball temperture & microstructure at ambient?  {iter_value}.')

    def incr_changed(self):
        incr: int = int(self.cbo_incr.itemData(self.cbo_incr.currentIndex()))
        self.input_dict.update({'incr': incr})
        logger.info(f'Print increment set to {incr}.')

    def create_dat_files(self):
        os.chdir(os.path.join(self.base_dir, 'DATFiles'))
        if platform == 'linux':
            qball2d = os.path.join(self.base_dir, 'Fortran/QBALL2D')
        elif platform == 'win32':
            qball2d = os.path.join(self.base_dir, 'Fortran/QBALL2D.exe')
        else:
            raise RuntimeError('The QBALL2D executable has only been compiled for Windows and Linux.')
        input_files = InputDATFile(self.base_dir, self.input_dict)
        ltemp = LTemFile(self.base_dir)
        phase = PhaseFile(self.base_dir)
        stress = StressFile(self.base_dir)
        temp = TempFile(self.base_dir)
        itt = ITTFile(self.base_dir)
        input_files.create_file()
        ltemp.create_file()
        phase.create_file()
        stress.create_file()
        temp.create_file()
        itt.create_file()
        subprocess.call(qball2d)

    def create_excel(self) -> Workbook:
        tempfile = os.path.join(os.path.join(self.base_dir, 'DATFiles'), 'TEMP.DAT')
        phasefile = os.path.join(os.path.join(self.base_dir, 'DATFiles'), 'PHASE.DAT')
        stressfile = os.path.join(os.path.join(self.base_dir, 'DATFiles'), 'STRESS.DAT')
        ltempfile = os.path.join(os.path.join(self.base_dir, 'DATFiles'), 'LTEMP.DAT')
        wb = Workbook()
        temp_process = TempProcess(workbook=wb, datfile=tempfile)
        wb = temp_process.read_datfile()
        phase_process = PhaseProcess(workbook=wb, datfile=phasefile)
        wb = phase_process.read_datfile()
        stress_process = StressProcess(workbook=wb, datfile=stressfile)
        wb = stress_process.read_datfile()
        ltemp_process = LTempProcess(workbook=wb, datfile=ltempfile)
        wb = ltemp_process.read_datfile()
        if 'Sheet' in wb.sheetnames:
            wb.remove(wb['Sheet'])
        return wb
