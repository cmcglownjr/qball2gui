import logging
from PySide2 import QtCore, QtWidgets
from PySide2.QtWidgets import QApplication, QMainWindow, QVBoxLayout, QGridLayout, QHBoxLayout, QSpacerItem, QWidget
from PySide2.QtWidgets import QTextBrowser, QPushButton, QComboBox, QLineEdit, QStatusBar, QMenuBar, QLabel, QAction
from PySide2.QtWidgets import QFileDialog, QDialogButtonBox, QTreeWidget, QTreeWidgetItem, QTreeWidgetItemIterator
from PySide2.QtCore import QFile, Qt, QSize
from PySide2.QtGui import QIcon, QFont, QPixmap

from lib.utilities import create_logger
from ITTDialog import ITTDialog

logger = create_logger(__name__)


class IttWindow(QWidget):
    def __init__(self, parent=None):
        super().__init__(parent=parent)
        self.setWindowTitle('Isothermal Transformation Temperature and Ball Chemistry')
        # Widgets
        self.button_box: QDialogButtonBox = QDialogButtonBox()
        # self.button_box.addButton("Load", QDialogButtonBox.ActionRole)
        # self.button_box.addButton("Save", QDialogButtonBox.ActionRole)
        self.button_box.addButton("Apply", QDialogButtonBox.ApplyRole)
        self.button_box.addButton("Cancel", QDialogButtonBox.RejectRole)
        self.button_box.accepted.connect(self.accepted)
        self.button_box.rejected.connect(self.close)
        # self.button_box.button('Load').click.connect(self.load_file)
        # self.button_box.button('Save').click.connect(self.save_file)
        self.ln_c: QLineEdit = QLineEdit()
        self.ln_mn: QLineEdit = QLineEdit()
        self.ln_si: QLineEdit = QLineEdit()
        self.ln_cr: QLineEdit = QLineEdit()
        self.ln_mo: QLineEdit = QLineEdit()

        self.steel_chem = [0.8, 0.9, 0.2, 0.6, 0.0, 0.07]
        self.iso_trans_tem = [
                [1300, 115, 300, 1230],
                [1250, 50, 100, 200],
                [1200, 22, 50, 90],
                [1150, 14.6, 30.3, 60],
                [1100, 10.7, 24.6, 50],
                [1050, 9, 23, 46],
                [1000, 8, 23, 46]
            ]
        # Layout
        main_layout: QVBoxLayout = QVBoxLayout()
        main_layout.addLayout(self.create_layout())
        main_layout.addWidget(self.button_box)
        # widget = QWidget()
        self.setLayout(main_layout)

    def create_layout(self) -> QVBoxLayout:
        lbl_steel: QLabel = QLabel('Steel Chemistry')
        lbl_itt: QLabel = QLabel('Isothermal Transformation Temperature')
        lbl_c: QLabel = QLabel('C:')
        lbl_mn: QLabel = QLabel('Mn:')
        lbl_si: QLabel = QLabel('Si:')
        lbl_cr: QLabel = QLabel('Cr:')
        lbl_mo: QLabel = QLabel('Mo:')

        chem_layout: QGridLayout = QGridLayout()
        chem_layout.addWidget(lbl_c, 0, 0)
        chem_layout.addWidget(self.ln_c, 0, 1)
        chem_layout.addWidget(lbl_mn, 0, 2)
        chem_layout.addWidget(self.ln_mn, 0, 3)
        chem_layout.addWidget(lbl_si, 0, 4)
        chem_layout.addWidget(self.ln_si, 0, 5)
        chem_layout.addWidget(lbl_cr, 0, 6)
        chem_layout.addWidget(self.ln_cr, 0, 7)
        chem_layout.addWidget(lbl_mo, 0, 8)
        chem_layout.addWidget(self.ln_mo, 0, 9)

        tree_itt: QTreeWidget = QTreeWidget()
        tree_itt.setColumnCount(4)
        tree_itt.setHeaderLabels(['ITT', 'T01', 'T50', 'T99'])

        btn_add: QPushButton = QPushButton()
        btn_add.setIcon(QIcon('Resource/Qball-Add-256.png'))
        btn_add.clicked.connect(self.add_item)
        btn_remove: QPushButton = QPushButton()
        btn_remove.setIcon(QIcon('Resource/Qball-Minus-256.png'))
        btn_remove.clicked.connect(self.remove_item)
        btn_layout: QVBoxLayout = QVBoxLayout()
        btn_layout.addWidget(btn_add)
        btn_layout.addWidget(btn_remove)

        itt_layout: QHBoxLayout = QHBoxLayout()
        itt_layout.addWidget(tree_itt)
        itt_layout.addLayout(btn_layout)

        layout: QVBoxLayout = QVBoxLayout()
        layout.addWidget(lbl_steel)
        layout.addLayout(chem_layout)
        layout.addWidget(lbl_itt)
        layout.addLayout(itt_layout)

        return layout

    def add_item(self):
        itt_list = []
        dialog = ITTDialog(itt_list)
        dialog.show()
        logger.debug(len(itt_list))

    def remove_item(self):
        pass

    def accepted(self):
        pass

    def load_file(self):
        pass

    def save_file(self):
        pass
