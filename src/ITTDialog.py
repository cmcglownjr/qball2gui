import logging
from PySide2 import QtCore, QtWidgets
from PySide2.QtWidgets import QVBoxLayout, QFormLayout, QWidget
from PySide2.QtWidgets import QTextBrowser, QPushButton, QComboBox, QLineEdit, QStatusBar, QMenuBar, QLabel
from PySide2.QtWidgets import QFileDialog, QDialogButtonBox
from PySide2.QtCore import QFile, Qt, QSize
from PySide2.QtGui import QIcon, QFont, QPixmap

from lib.utilities import create_logger


class ITTDialog(QWidget):
    def __init__(self, itt_list: list = None, parent=None):
        super().__init__(parent=parent)
        self.itt_list = itt_list
        self.setWindowTitle('Add Isothermal Transformation Temperature')
        q_btn = QDialogButtonBox.Save | QDialogButtonBox.Close

        button_box = QDialogButtonBox(q_btn)
        button_box.accepted.connect(self.accepted)
        button_box.rejected.connect(self.close)
        lbl_itt: QLabel = QLabel('ITT:')
        lbl_t01: QLabel = QLabel('T01:')
        lbl_t50: QLabel = QLabel('T50:')
        lbl_t99: QLabel = QLabel('T99:')
        self.ln_itt: QLineEdit = QLineEdit()
        self.ln_itt.setPlaceholderText('0')
        self.ln_t01: QLineEdit = QLineEdit()
        self.ln_t01.setPlaceholderText('0')
        self.ln_t50: QLineEdit = QLineEdit()
        self.ln_t50.setPlaceholderText('0')
        self.ln_t99: QLineEdit = QLineEdit()
        self.ln_t99.setPlaceholderText('0')
        form_layout: QFormLayout = QFormLayout()
        form_layout.addRow(lbl_itt, self.ln_itt)
        form_layout.addRow(lbl_t01, self.ln_t01)
        form_layout.addRow(lbl_t50, self.ln_t50)
        form_layout.addRow(lbl_t99, self.ln_t99)
        layout: QVBoxLayout = QVBoxLayout()
        layout.addLayout(form_layout)
        layout.addWidget(button_box)
        self.setLayout(layout)

    def accepted(self) -> list:
        self.itt_list = [float(self.ln_itt.text()), float(self.ln_t01.text()), float(self.ln_t50.text()),
                         float(self.ln_t99.text())]
        return self.itt_list
