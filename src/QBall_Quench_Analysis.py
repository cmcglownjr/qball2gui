import os
import sys
from pathlib import Path
import logging
from PySide2.QtWidgets import QApplication

from lib import utilities
from MainWindow import MainWindow


if __name__ == '__main__':
    base_dir = Path(__file__).resolve().parent.parent
    cwd = os.getcwd()
    if not os.path.exists(os.path.join(cwd, 'DATFiles')):
        os.mkdir(os.path.join(cwd, 'DATFiles'))
    logger = utilities.create_logger(__name__)
    app = QApplication(sys.argv)
    window = MainWindow()
    window.show()
    app.exec_()
